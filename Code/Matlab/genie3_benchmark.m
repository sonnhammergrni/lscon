% small script to remove diagonal from true network and flipping
% the genie3 predicted network in order to perform an accurate
% comparison between them.

% define a few values we will need to find all the datasets 
Ns=[100,300,500]
SNRs=[0.001,0.01,0.1,1];
datasets = 10
zetavec = logspace(-6,0,30);

% run the method on all datasets 
for n = 1:length(Ns)
    N = Ns(n)
    % first for data that will not form hubs
    for i = 1:datasets
        for z = 1:length(SNRs)
            
            SNR = SNRs(z)
            
            load strcat("2020-10-21_scalefree_data/N",N,"/dataset_scaleFreeNet",i,"_N",N,"_SNR_",SNR,".mat")

            true_max_deg = max(sum(net.A~=0));
            true_med_deg = mean(sum(net.A~=0));
            
            [result,f1_mat(i,:,1),pred_net(:,:,i)] = run_method('Genie3',pred_net(:,:,i),zetavec,"Original",net,SNR,true_max_deg,true_med_deg,i+1);
            print_var = [print_var; result];
        end
    end
    % then for data where hubs are present 
    for i = 1:datasets
        for z = 1:length(SNRs)
            SNR = SNRs(z)
            
            load strcat("2020-10-21_scalefree_data/hubby_N",N,"/dataset_scaleFreeNet",i,"_N",N,"_SNR_",SNR,".mat")

            true_max_deg = max(sum(net.A~=0));
            true_med_deg = mean(sum(net.A~=0));
            
            [result,f1_mat(i,:,1),pred_net(:,:,i)] = run_method('Genie3',pred_net(:,:,i),zetavec,"Hub_induced",net,SNR,true_max_deg,true_med_deg,i+11);
            print_var = [print_var; result];
        end
    end
end

% define a function that runs each method
function [results, f1_vals,outnet] = run_method(method,data,spars,origin,net,SNR,tmaxd,tmedd,point)
    % run the method 
    timer_start = cputime;
    hold = Methods.(method)(data,spars);
    elapsed_time = cputime-timer_start;

    % due to genespider evaluating networks from a oposite
    % direction to most standard models we flip the network to get
    % corresponding results 
    outnet = hold';

    % we also remove self regulation as genie3 does not account for
    % this by design 
    A = net.A;
    A(find(eye(size(A,1))))=0;

    % and finally find correctness 
    hold_M = analyse.per_edge_correctness_count(A,outnet);
    [~, p] = max(hold_M.F1);
    degree = hold_M.max_deg(p);
    m_degree = hold_M.medi_m_deg(p);

    [~,pos] = min(abs(tmedd-hold_M.medi_m_deg));
    medi_deg = hold_M.max_deg(pos);
    medi_m_deg = hold_M.medi_m_deg(pos);

    Mcoms = [1 hold_M.comspe 0]; Mpre = [0 hold_M.pre hold_M.pre(length(hold_M.pre))]; Msen = [1 hold_M.sen 0];
    AUPR = -trapz(Msen, Mpre); AUROC = -trapz(Mcoms, Msen);

    results = [AUPR, AUROC, max(hold_M.F1), degree, m_degree, origin, method, SNR, elapsed_time, tmaxd,medi_deg, medi_m_deg];

    f1_vals = hold_M.F1;

    while length(f1_vals)<size(A,1)*size(A,2)

        f1_vals = [f1_vals,0];

    end
end

