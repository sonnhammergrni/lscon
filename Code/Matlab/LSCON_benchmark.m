% script for benchmarking LSCONB. Note that GENIE3 is ran
% seperately due to time constrains 

% set zetavec to a standard 
zetavec = logspace(-6,0,30);
% set what sizes to run
Ns = [1000];
% set SNR level of files to run
SNRs = [.001, .01, .1, 1];
% set number of datasets to look for
sets = 5;
% specify where to load data from
from_load = "2020-10-21_scalefree_data";
% set the prefix on files to load
prefix = "dataset_scaleFreeNet";

for qq = 1:length(Ns)
    N = Ns(qq)
    output = strcat("LSCON_benchmark_regression_methods_N",num2str(N),".mat")		

    % initialize the save mat to ensure that the right headers are there
    print_var = ["AUPR","AUROC","max_F1","Max_degree_F1","Mean_degree_F1","Data_type","Method","SNR","Run_time","True_max_degree","Max_degree_med","Mean_degree_med"];

    fprintf("Running the %i nets",N)
    
    % run for none hubby data
    for (nn = 1:sets)
    
        for s = 1:length(SNRs)

	    SNR = SNRs(s);
            file = (strcat(from_load,"/N",num2str(N),"/",prefix,num2str(nn),"_N",num2str(N),"_SNR_",num2str(SNR),".mat"))
	    

            load(file)
	    true_max_deg = max(sum(net.A~=0));
            true_med_deg = nnz(net.A)/data.N;     
 	    	                
            % run lsco
            [result,f1_mat.Tlsco(:,nn,s), pred_net.Tlsco(:,:,s,nn)] = run_method('lsco',data,zetavec,"Original",net,SNR,true_max_deg,true_med_deg);
            print_var = [print_var; result];

            % run lasso
            [result,f1_mat.Tlasso(:,nn,s),pred_net.Tlasso(:,:,s,nn)] = run_method('Glmnet',data,zetavec,"Original",net,SNR,true_max_deg,true_med_deg);
            print_var = [print_var; result];

            % run LSCON
            [result,f1_mat.Tcon(:,nn,s), pred_net.Tlscon(:,:,s,nn)] = run_method('LSCON',data,zetavec,"Original",net,SNR,true_max_deg,true_med_deg);
            print_var = [print_var; result];

            % run ridgeco
            [result,f1_mat.Tridge(:,nn,s), pred_net.Tridge(:,:,s,nn)] = run_method('ridgeco',data,zetavec,"Original",net,SNR,true_max_deg,true_med_deg);
            print_var = [print_var; result];

        end
	save(output,'-V7.3')	
   end
   
   %run for hubby data 
   for (i = 1:sets)

        for s = 1:length(SNRs)
 
            file =(strcat(from_load,"/hubby_N",num2str(N),"/",prefix,num2str(i),"_hubby_N",num2str(N),"_SNR_",num2str(SNRs(s)),".mat"))

            load(file)

	    true_max_deg = max(sum(net.A~=0));
 	    true_med_deg = nnz(net.A)/data.N;     
             
            % run lsco
            [result,f1_mat.lsco(:,i,s), pred_net.Hlsco(:,:,s,i)] = run_method('lsco',data,zetavec,"Hub_induced",net,SNRs(s),true_max_deg,true_med_deg);
            print_var = [print_var; result];

            % run lasso
            [result,f1_mat.lasso(:,i,s), pred_net.Hlasso(:,:,s,i)] = run_method('Glmnet',data,zetavec,"Hub_induced",net,SNRs(s),true_max_deg,true_med_deg);
            print_var = [print_var; result];

            % run LSCON
            [result,f1_mat.lscon(:,i,s), pred_net.Hlscon(:,:,s,i)] = run_method('LSCON',data,zetavec,"Hub_induced",net,SNRs(s),true_max_deg,true_med_deg);
            print_var = [print_var; result];
                
            % run ridgeco
            [result,f1_mat.ridge(:,i,s), pred_net.Hridge(:,:,s,i)] = run_method('ridgeco',data,zetavec,"Hub_induced",net,SNRs(s),true_max_deg,true_med_deg);
            print_var = [print_var; result];
        end
        
        % save data as to ensure not all data is lost if a
        % crash happens 
        save(output,'-V7.3')
   end
   clear print_var;
   clear f1_mat;
   clear pred_net;
   clear output;
end

% define a function that runs each method
function [results, f1_vals,outnet] = run_method(method,data,spars,origin,net,SNR,tmaxd,tmedd)
	 
	 % as glmnet and ridgeco have to be ran in a special way check what is asked for. 
	 if strcmp(method,'Glmnet') | strcmp(method,'ridgeco')
	     timer_start = cputime;
    	     hold = Methods.(method)(data,spars);
    	     elapsed_time = cputime-timer_start;
             hold_M = analyse.calc_correctness_curves(net,hold);
                                                                                 
             [v p] = max(hold_M.F1);                                                                                                       
             degree = max(sum(hold(:,:,p)~=0));                                                
             m_degree = median(sum(hold(:,:,p)~=0));                                                                                         

             [~,pos] = min(abs(tmedd-hold_M.nlinks/data.N));                                           
             medi_deg = max(sum(hold(:,:,pos)~=0));                                             
             medi_m_deg = median(sum(hold(:,:,pos)~=0)); 
    	     
	     outnet = hold(:,:,1);		
			
	  else 	  
	     timer_start = cputime;	
    	     hold = Methods.(method)(data,spars);
    	     elapsed_time = cputime-timer_start;

	     hold_M = analyse.calc_correctness_curves(net,hold); 
	     
	     [~,p] = max(hold_M.F1);                                             
             degree = max(sum(hold(:,:,p)~=0));                                                
             m_degree = median(sum(hold(:,:,p)~=0));      

             [~,pos] = min(abs(tmedd-hold_M.nlinks/data.N));
	     medi_deg = max(sum(hold(:,:,pos)~=0));                                             
             medi_m_deg = median(sum(hold(:,:,pos)~=0)); 
    	     outnet = hold(:,:,1);				
	  end	    

    results = [hold_M.AUPR, hold_M.AUROC, max(hold_M.F1), degree, m_degree, origin, method, SNR, elapsed_time, tmaxd, medi_deg, medi_m_deg];
    f1_vals = hold_M.F1;

    while length(f1_vals) < size(outnet,1)*size(outnet,2)
    	  f1_vals = [f1_vals,0];
    end	  
end

