function M = per_edge_correctness_count(true_net, pred_net)
% function that takes a true network and a full estimated network
% and walks over the predicted network removing the smallest edge
% every time. If several edges is of similar size it will pick all
% of them

    % check if input network is a single network or a set of networks
    % as GS often stores multiple networks in the pred_net
    if size(pred_net,3) >1
        pred_net = pred_net(:,:,1);
    end
    
    true_bingraph = logical(true_net);

    zetavec = abs(pred_net(:)');
    zetavec = sort(unique(zetavec));

    for i = 1:length(zetavec) 

        % as to not focus to much on weights in the network as the
        % meaning of these wary between methods the network is made
        % in to a binary matrix 
        tmpA = pred_net;
        tmpA(find(abs(tmpA)<=zetavec(i))) = 0;
        bingraph = logical(tmpA); 

        % get the overlapping and none overlapping edges 
        M.nlinks(i) = nnz(bingraph);
        M.TP(i) = sum(sum(and(true_bingraph, bingraph)));
        M.TN(i) = sum(sum(and(~true_bingraph, ~bingraph)));
        M.FP(i) = sum(sum(and(~true_bingraph, bingraph)));
        M.FN(i) = sum(sum(and(true_bingraph, ~bingraph)));

        % add a check to ensure code won't crash due to division
        % with 0
        % All zero networks could with advantage be droped, this
        % however makes comparisons between different methods
        % harder so it has not been implemented here
        if nnz(bingraph) == 0
            M.pre(i) = 0; 
        else
            M.pre(i) = M.TP(i)/(M.TP(i)+M.FP(i));
        end

        % calculate all values needed for the F1, MCC and ROC
        % curves we want. 
        M.sen(i) = M.TP(i)/(M.TP(i)+M.FN(i));
                
        M.comspe(i) = M.FP(i)/(M.TN(i)+M.FP(i));

        n = 2*M.TP(i) + M.FP(i) + M.FN(i);
        % and then get F1 and MCC values 
        if n == 0
            M.F1(i) = 0;
        else
            M.F1(i) = 2*M.TP(i)/n;
        end

        n = (M.TP(i) + M.FP(i)) * (M.TP(i) + M.FN(i)) * (M.TN(i)+M.FP(i)) * (M.TN(i)+M.FN(i));
        if n == 0
            M.MCC(i) = 0;
        else
            M.MCC(i) = (M.TP(i)*M.TN(i)-M.FP(i)*M.FN(i))/sqrt(n);
        end
        
        M.max_deg(i) =  max(sum(bingraph~=0));
        M.medi_m_deg(i) = median(sum(bingraph~=0));

    end
    % finally use the calculated values to get the ROC curves 
    M.AUPR = calc_AUPR(M);
    M.AUROC = calc_AUROC(M);
end

function varargout = calc_AUROC(M)
% Calculate the area under the TPR/FPR curve
    TPR = M.sen;
    FPR = M.comspe;
    [~,ord] = sort(M.nlinks);
    TPR = TPR(ord);
    FPR = FPR(ord);
    
    auroc = trapz(FPR,TPR);
    
    varargout{1} = auroc;
end

function varargout = calc_AUPR(M)
% Calculate the au precision recall curve.
    PRE = M.pre;
    REC = M.sen;
    [~,ord] = sort(M.nlinks);
    PRE = PRE(ord);
    REC = REC(ord);
    
    aupr = trapz(REC,PRE);
    varargout{1} = aupr;
end

