function [hub_data hubs] = megahub_inserter(varargin)
  % function hubby_data = megahub_insert(data.Y, hub_size, silent)
  % hub_size can be small, medium, large given as "S","M" or "L"
  % note that due to the random nature of the script S,M or L -
  % increases likelyhood of the hubs being smaller or larger but can not guarantee it 
  
  % method for adding 1:max(0.05*data_size,1) hubs to a dataset
  % by dividing each element in randomly selected columns with
  % a set of N random drawn int values.

  % check if it should be silent
for i=1:nargin
   if isa(varargin{i},'datastruct.Dataset')
       data = varargin{i}.Y;
   elseif isa(varargin{i},'double')
        max_hub_rows = varargin{i};
    else
        if ~exist('hub_size')
            hub_size = varargin{i};
        else
            verbose = false;
        end
    end
end

  % get the maximum number of rows and columns in data
  genes = size(data,1);
  reps = length(data);

  % select either 5% of the genes (rows) or 1 as the maximum number of rows to change
  if ~evalin( 'base', 'exist(''max_hub_rows'',''var'') == 1' )
      max_hub_rows = max(floor(genes*0.05),10)

        % draw a random int in range 1 to max_hub_rows
        % this determines how many rows will be modified
        n_rows = randi(max_hub_rows,1);
  else
      n_rows = max_hub_rows;
  end
  n_rows = max_hub_rows;
  % determine how large hubs we want to generate
  if hub_size == "S"
    m = 0.05;
  elseif hub_size == "M"
    m = 0.2;
  else
    m = 0.5;
  end

  % initiate an empty vector to stor modified rows in
  moded_rows = [];
  % loop over the number of rows that should be changed 
  for row = 1:n_rows
      perm = randi([genes*m,(genes*m)+250],[1,reps]);
      
      % select a random row in the data to modify 
      mod_row = randi(genes,1);
      moded_rows(row) = mod_row;

      % perform element wise division on the selected row using perm
      data(mod_row,:) = data(mod_row,:)./perm;	
	
  end

  % if verbose is true print some info on what rows was modified  
  if ~evalin( 'base', 'exist(''verbose'',''var'') == 1' )
      txt = sprintf("\nAdded %s megahubs to the data,",int2str(n_rows));
      disp(txt)
      fmt = ['in the rows: [', repmat('%g, ', 1, numel(moded_rows)-1), '%g]\n'];
      txt = sprintf(fmt,moded_rows);
      disp(txt)
  end
   
  % feed the now modifed data to the output variable
  hub_data = data;
  hubs = n_rows;
