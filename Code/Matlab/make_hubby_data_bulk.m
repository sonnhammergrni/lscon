% script for making and saving hubby genespider data in bulk 
% note that settings are here the same as for the LSCON paper and should be updated if used further 
% for smaller use of to write your own version of this the script relies entierly on the megahub_inserter.m script in the same folder. 


Ns = [100,300,500,800];
SNRs = [0.001, 0.01, 0.1 1];
path = ""; % where to find the input
prefix = "/dataset_scaleFreeNet"; % a pattern to look for in the input file names 
outdir = ""; % where to send the output

% on average what size should the added hubs be
hub_size = "M";
% how many percental hubs should be added
add_hubs = 10;

for q = 1:length(Ns)
    N = Ns(q)

    for (z = 1:10)

        for s = 1:length(SNRs)
            % load the data corresponding to each loop
            load(strcat(path,"N",num2str(N),"/",prefix,num2str(z),"_N",num2str(N),"_SNR_",num2str(SNRs(s)),".mat"));

            % add the megahubs to the data 
            data.Y= megahub_inserter(data, add_hubs, hub_size,"silent"); 

            % dynamically generate name for file and save path
            % based on what loop we are in 
            name = strcat(prefix,num2str(z),"_hubby_N",num2str(N),"_SNR_",num2str(SNRs(s)),".mat");
            out = strcat(outdir,"hubby_N",num2str(N));

            % if the output directory does not exist create it
            if ~exist(out,'dir')
                mkdir(out);
            end
            % finally save the data to out path under the generated            
            fullname = fullfile(out,name);
            save(fullname, "data", "net")
            
        end
    end
end
