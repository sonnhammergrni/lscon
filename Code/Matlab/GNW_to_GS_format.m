% script for reading in geneNetWeaver data in triplicates to matlab and applying a normalisation

close all hidden; close all; clear all; clc
addpath(genpath('~/GS'))
addpath(genpath('~/GS/genespider'))
addpath(genpath('~/SubsetSelection'))

% not that the script assums that path contains:
% prefix-1_knockdowns.tsv
% prefix-1_nonoise_knockdowns.tsv
% prefix-1_wildtype.tsv
% prefix-1_goldstandard_signed.tsv

% define a set of variables to load the data
halfpath = "~/LESSHUB/GNWdata/FullFiles/N";

sizes = [100, 150, 200];
sets = [1,2,3,4,5];

for kk = 1 %:length(sizes)
  for mm = 1:length(sets)
    save_name = strcat("~/LESSHUB/GNWdata/GNW_LESSHUB_N", num2str(sizes(kk)));

    prefix = strcat('Ecoli-', num2str(sets(mm)));
    path = strcat(halfpath, num2str(sizes(kk)), '/set', num2str(sets(mm)));

    % load the data to varialbes
    R1 = readtable(strcat(path,"/rep1/",prefix,"_knockdowns.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1);
    names = R1.Properties.VariableNames'; R1 = table2array(R1);
    R2 = readtable(strcat(path,"/rep2/",prefix,"_knockdowns.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); R2 = table2array(R2);
    R3 = readtable(strcat(path,"/rep3/",prefix,"_knockdowns.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); R3 = table2array(R3);
    wt1 = readtable(strcat(path,"/rep1/",prefix,"_wildtype.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); wt1 = table2array(wt1);
    wt2 = readtable(strcat(path,"/rep2/",prefix,"_wildtype.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); wt2 = table2array(wt2);
    wt3 = readtable(strcat(path,"/rep3/",prefix,"_wildtype.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); wt3 = table2array(wt3);

    % noise-free
    N1 = readtable(strcat(path,"/rep1/",prefix,"_nonoise_knockdowns.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); N1 = table2array(N1);
    N2 = readtable(strcat(path,"/rep2/",prefix,"_nonoise_knockdowns.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); N2 = table2array(N2);
    N3 = readtable(strcat(path,"/rep3/",prefix,"_nonoise_knockdowns.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); N3 = table2array(N3);

    NW1 = readtable(strcat(path,"/rep1/",prefix,"_nonoise_wildtype.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); NW1 = table2array(NW1);
    NW2 = readtable(strcat(path,"/rep2/",prefix,"_nonoise_wildtype.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); NW2 = table2array(NW2);
    NW3 = readtable(strcat(path,"/rep3/",prefix,"_nonoise_wildtype.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); NW3 = table2array(NW3);

    %% Noise-free fold change
    NF1 = zeros(size(R1));
    NF2 = zeros(size(R2));
    NF3 = zeros(size(R3));

    for i = 1:size(R1,1)
      NF1(:,i) = log2(abs(N1(i,:)./NW1)).*sign(N1(i,:)./NW1);
      NF2(:,i) = log2(abs(N2(i,:)./NW2)).*sign(N2(i,:)./NW2);
      NF3(:,i) = log2(abs(N3(i,:)./NW3)).*sign(N3(i,:)./NW3);
    end
    clear i

    %% Noise fold change
    No1 = R1 - N1; No2 = R2 - N2; No3 = R3 - N3;
    NoWt1 = wt1 - NW1; NoWt2 = wt2 - NW2; NoWt3 = wt3 - NW3;
    Noi1 = zeros(size(R1));
    Noi2 = zeros(size(R1));
    Noi3 = zeros(size(R1));

    for i = 1:size(Noi1,1)
      Noi1(:,i) = log2(abs(No1(i,:)./NoWt1)).*sign(No1(i,:)./NoWt1);
      Noi2(:,i) = log2(abs(No2(i,:)./NoWt2)).*sign(No2(i,:)./NoWt2);
      Noi3(:,i) = log2(abs(No3(i,:)./NoWt3)).*sign(No3(i,:)./NoWt3);
    end

    N = size(Noi1,1);
    P = [-eye(N), -eye(N), -eye(N)];
    E = [Noi1, Noi2, Noi3];
    %% Normalize E
    NormE = zeros(size(E));
    for i=1:N
      col = find((P(i,:)));
      tmp = E(:,col);
      m = mean(tmp(:)); sd = std(tmp(:));
      Ytmp = (tmp-m)/sd;
      NormE(:,col) = Ytmp;
    end

    data_mat = [NF1, NF2, NF3];
    %% Normalize noise-free foldchange
    X = zeros(size(data_mat));
    for i=1:N
      col = find((P(i,:)));
      tmp = data_mat(:,col);
      m = mean(tmp(:)); sd = std(tmp(:));
      Ytmp = (tmp-m)/sd;
      X(:,col) = Ytmp;
    end

    Y = X + NormE;

    %% make a dataset from the generated data
    stdY = std((Y),0,2);
    Yvar = stdY.^2;
    reps=sum(P~=0,2);
    lambda = sum((reps'-1)*Yvar(:)/((reps-1)*length(Yvar(:))),'omitnan');
    lambdas = (Yvar');%./(size(Yvar,2));
    F = zeros(size(P));

    D.E = NormE;
    D.F = F;
    D.Y = Y;
    D.P = P;
    D.lambda = [lambda, 0];
    D.sdY = repmat(stdY,1,length(Y));
    D.sdP = zeros(size(D.sdY));
    D.cvY = diag(lambdas);
    D.cvP = zeros(size(D.cvY));

    data(mm) = datastruct.Dataset(D);

    %% this comes from R
    Network = readtable(strcat(halfpath, num2str(sizes(kk)), '/set', num2str(mm), '/rep1/', "EcoliTrueGRN.csv"),'Delimiter', ',', 'ReadVariableNames', 1);
    Network = table2array(Network);
    Network(find(eye(N))) = 1;
    net(mm) = datastruct.Network(Network);
    net(mm).names = names;
  end
  save(save_name, "data", "net")
end
