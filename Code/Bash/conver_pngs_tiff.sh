#!/bin/bash

# set the directory to convert images is 
imgdir="/home/thomash/projects/LSCON/Images/figures_in_paper/pngs"
cd $imgdir

# convert all png images to tif. This keeps the png image and creates a new tif image
for f in *.png; do convert "$f" "${f%%.*}.tif"; done

