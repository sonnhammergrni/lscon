import sys
import numpy as np
import pandas as pd 
import seaborn as sns
import matplotlib.pyplot as plt

out = False

# load data and rename some things
infile1 = sys.argv[1]
if len(sys.argv) > 2:
    out = sys.argv[2]    

df = pd.read_csv(infile1)
df = df.replace("ridgeco","RidgeCO")
df = df.replace("Glmnet","LASSO")

# sort the methods in a specific order so they can be assigned a specific color 
df["Method"] = pd.Categorical(df["Method"],["LSCON","LASSO","LSCO","RidgeCO","Genie3"])
df.sort_values(by="Method",ascending=False)
colors = ["green","gold","skyblue","darkorange","silver"]

# round the data to 4 decimals so the Y axis don't go crazy
# note as the smallest SNR is 0.001 we take 4 here to makes sure we don't screw with that value  
df = df.round(4)

# for the 2 datatypes non-hubby/hubby we make a plot each  
for i in df["Data_type"].unique():

    # set some seaborn parameters to ensure size and font size is right 
    sns.set_context("poster")
    sns.set(font_scale=3)
    sns.set_style("white")
    sns.set_style("ticks",{"ytick.major.size":8})
    # make the first plot for AUPR 
    g= sns.catplot(x="SNR",y="AUPR",hue="Method",col="Size",col_wrap=2,data=df.loc[df["Data_type"]==i],kind="bar",legend=False,palette=colors,legend_out=True,errwidth=2, capsize=.1)

    # set up and plot legend outside the main plot 
    g.set_xticklabels(rotation=20)
    axes = g.axes.flatten()
    g.fig.set_size_inches(20,20)

    plt.legend(bbox_to_anchor=(1, 1))
    plt.tight_layout()

    # and save it 
    if out:
        plt.savefig(out+"_"+i+"_AUPR.svg")
    else:
        plt.show()

    # close the plot to ensure we don't draw the next one ontop of this 
    plt.close()

    # do all the same things but this time for AUROC 
    sns.set_context("poster")
    sns.set(font_scale=3)
    sns.set_style("white")
    sns.set_style("ticks",{"ytick.major.size":8})    
    
    g= sns.catplot(x="SNR",y="AUROC",hue="Method",col="Size",col_wrap=2,data=df.loc[df["Data_type"]==i],kind="bar",legend=False,palette=colors,legend_out=True,errwidth=2, capsize=.1)
    g.set_xticklabels(rotation=35)
    g.set_axis_labels("SNR", "AUROC")

    g.fig.set_size_inches(20,20)

    plt.legend(bbox_to_anchor=(1, 1))
    plt.tight_layout()

    if out:
        plt.savefig(out+"_"+i+"_AUROC.svg")
    else:
        plt.show()

