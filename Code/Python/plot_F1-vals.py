import sys
import numpy as np
import pandas as pd 
import seaborn as sns
import matplotlib.pyplot as plt

out = False

# read in data
infile1 = sys.argv[1]
if len(sys.argv) > 2:
    out = sys.argv[2]    

df = pd.read_csv(infile1)
df = df.round(4)

# rename and reorder things for a nicer plot
df = df.replace("ridgeco","RidgeCO")
df = df.replace("Glmnet","LASSO")

df["Method"] = pd.Categorical(df["Method"],["LSCON","LASSO","LSCO","RidgeCO","Genie3"])
df.sort_values(by="Method",ascending=False)
colors = ["green","yellow","skyblue","darkorange","silver"]

# set plot parameters 
sns.set_context("poster")
sns.set(font_scale=3)
sns.set_style("white")
sns.set_style("ticks",{"ytick.major.size":8})

# and make the actual plot
g = sns.catplot(x="SNR",y="max_F1",hue="Method",col="Size",col_wrap=2,data=df.loc[df["Data_type"]=="Hub_induced"],kind="bar",palette=colors)

g.set_ylabels("max(F1)")
g.set_xticklabels(rotation=20)
axes = g.axes.flatten()
g.fig.set_size_inches(20,20)

if out:
    plt.savefig(out+"_maxF1_hubby.svg")
else:
    plt.show()
        
plt.close()

# a second plot is made for F1 values in non-hubby data
sns.set_context("poster")
sns.set(font_scale=3)
sns.set_style("white")
sns.set_style("ticks",{"ytick.major.size":8})

g = sns.catplot(x="SNR",y="max_F1",hue="Method",col="Size",col_wrap=2,data=df.loc[df["Data_type"]=="Original"],kind="bar",palette=colors)

g.set_ylabels("max(F1)")
g.set_xticklabels(rotation=20)
axes = g.axes.flatten()
g.fig.set_size_inches(20,20)

if out:
    plt.savefig(out+"_maxF1_original.svg")
else:
    plt.show()
        
plt.close()
