"""
Script for running a wilcoxon signed rank test between each unique method in a csv with the columns Method, AUPR and AUROC. Will return a matrix with the p-value between each method for the difference in AUPR and AUROC. 
"""

import sys
import numpy as np
import pandas as pd 
import rpy2.robjects as rob
from rpy2.robjects import pandas2ri


# Defining the R script and loading the instance in Python
r = rob.r
r['source']('ranksumtest.R')

# Loading the function we have defined in R.
ranksum = rob.globalenv['ranksumtest']

infile1 = sys.argv[1]

#if len(sys.argv) > 2:
#   out = sys.argv[2]    

df = pd.read_csv(infile1)
df = df.loc[df["Method"]!="LESSHUB2"]
df = df.loc[df["Method"]!="lsco"]
df = df.loc[df["Dataset"]<6]

df3 = pd.DataFrame(columns=["Method1","Method2","SNR","Size","P-value"])
# this code is somewhat terrible due to multiple forloops but it was fast to write
# first we do the check for each size 
for j in df["Size"].unique():
    for z in df["SNR"].unique():
        df2 = pd.DataFrame()
        # for each size we pick out the AUPR values for each method
        for i in df["Method"].unique():
            data = (df["AUPR"].loc[(df["Method"]==i) & (df["SNR"]==z) & (df["Data_type"]=="Original") & (df["Size"]==j)])
            data = data.reset_index(drop=True)
            df2[i] = data

        df2 = df2.fillna(0)
        pvals = pd.DataFrame(index=df2.columns,columns=df2.columns)

        # then we run a wilcoxon between each method and build a p-value matrix between each 
        for met1 in df2.columns:
            for met2 in df2.columns:
                if met1 == met2:
                    pval = 1
                else:
                    # run the actual test in R as the python version is unstable for n<20
                    set1 = rob.vectors.FloatVector(df2[met1])
                    set2 = rob.vectors.FloatVector(df2[met2])
                    pval = ranksum(set1,set2)[0]

                P_series = pd.Series({"Method1":met1,"Method2":met2,"SNR":z,"Size":j,"P-value":pval})
                df3 = df3.append(P_series,ignore_index=True)
                    
df3.to_csv(sys.argv[2],index=False)
        
