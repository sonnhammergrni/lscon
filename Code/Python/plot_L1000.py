# script for plotting the degree distrubution in L1000 data for the LESSHUB paper 2021-01
# takes as input a csv with each node out degree in one column and the corresponding method in the other column

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns 
import sys

def sorter(column):
    reorder = ["lsco","ridgeco","LSCON","Glmnet"]
    cat = pd.Categorical(column, categories=reorder, ordered=True)
    return pd.Series(cat)

indata = sys.argv[1]

# set the seaborn variables to correspond to the other figures in the paper
sns.set_context("poster")
#sns.set(font_scale=3)
sns.set_style("white")
sns.set_style("ticks",{"ytick.major.size":8})

plt.figure(figsize=(10,10))

df= pd.read_csv(indata)
df.columns=["Method","Degree"]

df["Degree"] = pd.to_numeric(df["Degree"])

df = df.sort_values("Method",key=sorter)

df = df.replace("Glmnet","LASSO")
df = df.replace("lsco","LSCO")
df = df.replace("ridgeco","RidgeCO")

colors = ["skyblue","darkorange","green","yellow"]

sns.boxenplot(x="Method", y="Degree",data=df,palette=colors)

plt.xlabel("")
plt.ylabel("Out degree")
plt.ylim(0,150)

if len(sys.argv) > 2:
    plt.savefig(sys.argv[2])
else:
    plt.show()
