# script for plotting the correctness in predctions from GNW data
# this is a seperate script from the GeneSPIDER data as the save format is different. 

import sys
import numpy as np
import pandas as pd 
import seaborn as sns
import matplotlib.pyplot as plt

out = False

# read command line input 
infile1 = sys.argv[1]
if len(sys.argv) > 2:
    out = sys.argv[2]    

# read and sort the data 
df = pd.read_csv(infile1)
df = df.round(4)


# replace some name so that the plotted names and the paper names are the same 
# there names also are more correct than the once sed during runs 
df = df.replace("ridgeco","RidgeCO")
df = df.replace("Glmnet","LASSO")
df = df.replace("lsco","LSCO")

df["Method"] = pd.Categorical(df["Method"],["RidgeCO","LSCO","LASSO","LSCON","Genie3"])
df.sort_values(by="Method",ascending=False)
colors = ["green","yellow","skyblue","darkorange","silver"]

# set a list of relatively well contrasting colors to use in the plot 
colors = ["darkorange","skyblue","green","yellow","silver"]

# set some values to get the plot to be large enough 
sns.set_context("poster")
sns.set(font_scale=3)
sns.set_style("white")
sns.set_style("ticks",{"ytick.major.size":8})

# plot AUPR 
g= sns.catplot(x="Method",y="AUPR",data=df,kind="bar",legend=False,palette=colors,legend_out=True,errwidth=2, capsize=.1)
g.set_axis_labels(" ", "AUPR")
axes = g.axes.flatten()
g.fig.set_size_inches(20,10)
plt.tight_layout()
if out:
    plt.savefig(out+"_AUPR.svg")
else:
    plt.show()

plt.close()

# We repeat the same thing for AUROC. 
sns.set_context("poster")
sns.set(font_scale=3)
sns.set_style("white")

g= sns.catplot(x="Method",y="AUROC",data=df,kind="bar",legend=False,palette=colors,legend_out=True,errwidth=2, capsize=.1)

g.set_axis_labels(" ", "AUROC")
axes = g.axes.flatten()
g.fig.set_size_inches(20,10)
plt.tight_layout()

if out:
    plt.savefig(out+"_AUROC.svg")
else:
    plt.show()

plt.close()
