"""
Small script to create a scatter plot of row sums in Y and column sums of A for comparisons
"""
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd 
import numpy as np
import sys

sns.set_context("poster")

df = pd.read_csv(sys.argv[1])

fig = plt.figure(figsize=(10,10))

sns.scatterplot(x="absolut row sum fold change data",y="Absolute column sum pred network",data=df,color="skyblue",edgecolor="k")

plt.ylabel("Absolute column sum in predicted GRN")
plt.xlabel("Absolute row sum in fold change data")
plt.ylim(0,80)
plt.xlim(0)

if len(sys.argv)>2:
    plt.savefig(sys.argv[2])
else:
    plt.show()
