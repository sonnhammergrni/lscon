import sys
import pandas as pd 
import seaborn as sns
import matplotlib.pyplot as plt
import os 

# read in data
infile1 = sys.argv[1]

if len(sys.argv) > 2:
    out = sys.argv[2]    
df = pd.read_csv(infile1)

size = infile1.split(".")[0].split("_")[-1]

# set some plot parameters to ensure the figure is readable 
sns.set_context("poster")
sns.set(font_scale=3)
sns.set_style("white")

sns.set_style("white",
 rc={
  'axes.spines.bottom': True,
  'axes.spines.left': True,
  'axes.spines.right': True,
  'axes.spines.top': True,
 }
)

df = df.round(4)

# rename a few things to make it clearer what we are talking about 
df = df.replace("ridgeco","RidgeCO")
df = df.replace("Glmnet","LASSO")
df = df.replace("lsco","LSCO")

# set colors and method order so that the color coresponds to other figures
# here we also ensure that the methods come in a falling order in the legend, slowest on top
# to make the figure easy to read 
Morder=["Genie3","LASSO","RidgeCO","LSCO","LSCON"]
colors = ["darkgrey","#FFCC33","darkorange","skyblue","green"]
df["Method"] = pd.Categorical(df["Method"],Morder)
df.sort_values(by="Method",ascending=False)

# sort methods on size 
df["Size"] = pd.to_numeric(df["Size"])
df.sort_values(by="Size",ascending=False)

# finally make the figures,
# first on for non-hubby data

plt.figure(figsize=(20,20))
g=sns.pointplot(x="Size",y="Run_time",hue="Method",data=df.loc[df["Data_type"]=="Original"],legend=True,legend_out=True,palette=colors,linewidth=1, capsize=0.2)

plt.setp(g.collections, sizes=[500])
plt.xlabel("Number of genes")
plt.ylabel("Run time (s)")

plt.yscale("log")
plt.legend(loc='best',markerscale=1)

if len(sys.argv) > 2:
    plt.savefig(out+"_original.svg")
else:
    plt.show()

# and then the same figure for hubby data 
    
plt.figure(figsize=(20,20))
g=sns.pointplot(x="Size",y="Run_time",hue="Method",data=df.loc[df["Data_type"]=="Hub_induced"],legend=True,legend_out=True,palette=colors,linewidth=1,capsize=0.2)

plt.setp(g.collections, sizes=[500])
plt.xlabel("Number of genes")
plt.ylabel("Run time (s)")

plt.yscale("log")
plt.legend(loc='best',markerscale=1)

if len(sys.argv) > 2:
    plt.savefig(out+"_hubinduced.svg")
else:
    plt.show()

