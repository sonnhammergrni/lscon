import sys
import numpy as np
import pandas as pd 
import seaborn as sns
import matplotlib.pyplot as plt

out = False

# read in data
infile1 = sys.argv[1]
if len(sys.argv) > 2:
    out = sys.argv[2]    
df = pd.read_csv(infile1)

# rename and order things to make the plot nicer 
df = df.replace("ridgeco","RidgeCO")
df = df.replace("Glmnet","LASSO")

# sort methods so that each one can have the same color in all plots 
df["Method"] = pd.Categorical(df["Method"],["LSCO","RidgeCO","LASSO","LSCON","Genie3"])
df.sort_values(by="Method",ascending=False)
colors = ["skyblue","darkorange","yellow","green","silver"]

# for each size make a seperate plot. This is done as the hubbines is hard to get a feel from if the Y axis is shared,
# what is a hub in 100 genes are not a hub in 800 genes 
for i in df["Size"].unique():

    # set plot variables 
    sns.set_context("poster")
    sns.set(font_scale=2)
    sns.set_style("white")
    sns.set_style("ticks",{"ytick.major.size":8})
    
    # get the size we want in to a seperate dataframe
    # that we will then subselect further from
    df2 = df.loc[df["Size"]==i]
    true_mean = np.median(df2["True_max_degree"].unique())

    # make a plot for data with hubs 
    g= sns.catplot(x="SNR",y="Max_degree_med",hue="Method",data=df2.loc[df2["Data_type"]=="Hub_induced"],kind="bar",legend=False,palette=colors,legend_out=True,errwidth=2,capsize=.1)

    # set labels 
    g.set_xticklabels(rotation=20)
    g.set_ylabels("Max out degree")
    g.fig.set_size_inches(10,10)

    # make a dotted line that corresponds to the maximum outdegree of the true netwok 
    axes = g.axes.flatten()
    axes[0].axhline(true_mean,ls="--",color="k",label="Median true\nmax degree")
    plt.legend(bbox_to_anchor=(1, 1))
    plt.tight_layout()
    plt.ylim(0,i)
    
    if out:
        plt.savefig(out+"_N"+str(i)+"_max_deg_closests_to_true_median_hubby.svg")
    else:
        plt.show()
        
    plt.close()

    # do the same thing for original data
    # this is primarily a sanity check as no methods produce hubs here 
    sns.set_context("poster")
    sns.set(font_scale=2)
    sns.set_style("white")
    sns.set_style("ticks",{"ytick.major.size":8})
        
    g= sns.catplot(x="SNR",y="Max_degree_med",hue="Method",data=df2.loc[df2["Data_type"]=="Original"],kind="bar",legend=False,palette=colors,legend_out=True,errwidth=2,capsize=.1)
    g.set_xticklabels(rotation=35)
    g.set_axis_labels("SNR", "Max out degree")

    g.fig.set_size_inches(10,10)

    axes = g.axes.flatten()
    axes[0].axhline(true_mean,ls="--",color="k",label="Median true\nmax degree")
    plt.legend(bbox_to_anchor=(1, 1))
    plt.tight_layout()
    plt.ylim(0,i)
    
    if out:
        plt.savefig(out+"_N"+str(i)+"_max_deg_closests_to_true_median_original.svg")
    else:
        plt.show()
    
