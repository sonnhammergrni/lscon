# Least Squares Cut-Off with Normalisation

This folder contains all code and data used to run the LSCON tool and generate images or perform analysis for the LSCON paper. Note that to use the method you will need the genespider package avalibel at: https://bitbucket.org/sonnhammergrni/genespider 
this directory instead contains the code for analysing and benchmarking the tools. If you wish to simply run it you can do so by downloading the geneSpider repo which contains a copy of the LSCON code. 


The work here presents a normalization based solution to least squares overfitting of infitesimal values that in gene regulatory network inference results in hyper conencted nodes. 
The method was developed to decrease impact of isolated genes or failed experimental perturbations in data as this causes fold change values to be close to but not equal to zero. 


Most folders are subdivided so that:

- code contains all code both for python and matlab
- data contains all data used as well as a a special folder called mat contaning mat files
- results contain the results for the paper. 